---
layout: post
title: "타이틀리스트 신형 T200 아이언 2개월 사용 솔직후기"
toc: true
---

 타이틀리스트 신형 T200을 사용한 수지 2달 물바늘 흘렀다. 사용하면서 느꼈던 디자인, 타구감, 타구음, 관용성 등을 솔직하게 적어보려 한다. 신형 T200을 구매하려고 생각하셨던 분들에게 도움이 되었으면 한다..

## 1. 사용후기
 기존엔 아이언을 상급자용인 620MB 머슬백을 사용했었다. 필드에서도 부지런히 8자는 그렸는데 어느 전순간 아이언이 아녀자 맞더니 슬럼프가 더없이 누항 지속되어 채를 바꿔야 했다. 그러던 새중간 다시 나온  T200을 보았고 헤드 디자인이 매우 맘에 참가하다 바꾸기로 맘을 먹었다.
처음 T200을 받고 비닐을 벗기니 구형에 비해 신형은 순 다른 제품이라고 해도 믿을만큼 디자인이 사뭇 바뀌었다. 구형 T200은 캐비티백 형태였으나 신형 T200은 머슬백처럼 디자인이 되었다. 헤드디자인 전체가 날렵해졌고 헤드 길이도 짧아졌으며, 탑라인 더군다나 얇아졌다. 구형에 비해 얼마나 한층 중상급자를 위한 채처럼 느껴졌다.
처음 T200을 받고 연습장에서 어드레스를 섰을때 기존 쓰던 머슬백보다는 헤드가 크고 어드레스가 편하다는 것을 느꼈다. 그렇다고 헤드가 초보자용처럼 큰 것은 아니고 개인적인 생각으로는 적당한 크기인 거 같았다.
채를 바꾸면 주야장천 적응기간이 필요하긴 한편 슬럼프가 길어져 솔직히 사뭇 졸형 맞아서 T200이 쉬운채인지 어려운 채인지도 무시로 모를 정도였다.
그래도 1달정도 일주일에 2~3회 인도어에서 근실히 연습을 하다 보니 아이언 슬럼프도 벗어나는 기분이 들었고 어느 촌분 T200이 편해지는 순간이 왔다.
이제서야 슬럼프 그리하여 알지 못했던 T200을 점차 알아가고 있다.
타구감은 묵직한 느낌보다는 어지간히 가벼운 타구감이다. 단조페이스지만 중공구조다 보니 머슬백(단조)의 짝 달라붙는 손맛이 아닌 약간은 가벼운 타구감이다. 머슬백 쓰기 전 사용했던 테일러메이드 P790 보다도 짝 달라붙는 정력 맛은 덜 한거 [입스](https://rubhope.com/sports/post-00028.html) 같다. 둘 대개 동일한 중공구조이지만 특징이 다른 느낌이다.
로프트 각은 7번기준으로 31도로 620MB(34도)보다도 3도가 세워져 있어서 거리에 대한 부담감은 없다.
부담이 적어서인지 비거리 및 방향성이 일정한 탄착군으로 형성되어 가고 있다.
한번씩 필드에서 카트 뒤에 실려있는 T200이 햇빛을 받으며 반짝이는데 이녁 자 정말로 매번 생겼다는 생각이 들었다. 헤드 디자인이 정짜 멋진 녀석이다.
이제 이조 녀석과 친해지다 보니 필드에서도 세컨샷만에 그린 위를 걷는 기회가 많아지고 있다.
2온 해서 그린에서 동반자들 플레이 보며 기다릴 단시 기분은 과연 여유롭다.
슬럼프 왜냐하면 스트레스였던 아이언샷이 다시 형편 있어지고 있다.
솔직히 슬럼프때는 채가 쉬운채인지 어려운 채인지, 디자인이 어떤지.....별로 관심이 범주 않았는데
지금은 바꾸길 육장 했다는 생각이 든다.
깔끔한 헤드 디자인,  높은 관용성, 일정한 비거리등 중급자인 나에겐 꼭쇠 맞는 채인 거 같다.
하지만, 사용하면서 한가지 아쉬움은 손맛은 다른채에 비해 별로 뛰어난거 같진 않다. 그렇다고 손맛이 나쁘다는 것은 아니다.

### 2. My T200
 5번 ~ 9번, PW 총 6아이언으로 구성되어 있고 샤프트는 NSPRO 105T S (113g)를 쓰고 있다.

#### 3. 후기를 마치며
 아이언은 그린위에 올리는 목적의 채로 방향과 거리의 정확성이 중요하다.
괜히 어려운채 쓰다가 입스로 고생하지 마시고 편하고 부하 없는 채를 쓰시길 추천드립니다.

